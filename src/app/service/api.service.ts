import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ChatacterDataContainer } from './apiModel/CharacterDataContainer';
import { Team } from './apiModel/Team';

@Injectable()
export class ApiService {
    private endpoint: String = 'http://localhost:8081/'
  constructor(private http: HttpClient) { }

  public  getHeroes(): Observable<ChatacterDataContainer> {
    return this.http.get<ChatacterDataContainer>(this.endpoint + "heroes?offset=0&limit=20")
  }

  public addToTeam(id: String): Observable<Team> {
      return this.http.put<Team>(this.endpoint + "MyHeroes/" + id, null);
  }

  public removeFromTeam(id: String): Observable<Team> {
    return this.http.delete<Team>(this.endpoint + "MyHeroes/" + id);
}

  public getTeam(): Observable<Team> {
    return this.http.get<Team>(this.endpoint + "MyHeroes");
}
}