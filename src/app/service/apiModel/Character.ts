export class Character {
    public id : String;
    public name : String;
    public description : String;
    public thumbnail: {
        path: String,
        extension: String
    }
}