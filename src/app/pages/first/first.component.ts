import { Component, Input } from '@angular/core';
import { HeroModel } from '../../model/heroModel';
import { ApiService } from '../../service/api.service';

@Component({
  selector: 'firstPage',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})

export class FirstPage {
    constructor(private apiService : ApiService){}

    private switchPage: boolean = false;

    private heroes: Array<HeroModel> = new Array(0);
    private team: Array<HeroModel> = new Array(0);
    
    
      ngOnInit() {
        this.apiService.getHeroes().subscribe(characterDataContainer => {characterDataContainer.results.forEach(result => {
            this.heroes.push(new HeroModel(result.id,result.name,result.thumbnail.path + "/portrait_xlarge." + result.thumbnail.extension,result.description));
        })
      });
      this.apiService.getTeam().subscribe(characterDataContainer => {characterDataContainer.myHeroes.forEach(result => {
        this.team.push(new HeroModel(result.id,result.name,result.thumbnail.path + "/portrait_xlarge." + result.thumbnail.extension,result.description));
        })
  });
      
  }

  private addToTeam(id: String): void {
      this.team = new Array(0);
    this.apiService.addToTeam(id).subscribe(characterDataContainer => {characterDataContainer.myHeroes.forEach(result => {
        this.team.push(new HeroModel(result.id,result.name,result.thumbnail.path + "/portrait_xlarge." + result.thumbnail.extension,result.description));
    })
  });
    
  }

  private removeFromTeam(id: String): void {
    this.team = new Array(0);
    this.apiService.removeFromTeam(id).subscribe(characterDataContainer => {characterDataContainer.myHeroes.forEach(result => {
        this.team.push(new HeroModel(result.id,result.name,result.thumbnail.path + "/portrait_xlarge." + result.thumbnail.extension,result.description));
    })
  });
    
  }

}