import { Component, Input } from '@angular/core';

@Component({
  selector: 'heroDisplay',
  templateUrl: './heroDisplay.component.html',
  styleUrls: ['./heroDisplay.component.css']
})

export class HeroDisplay {


    @Input() name: String = '';

    @Input() url: String = '';

    @Input() description: String = '';
  }