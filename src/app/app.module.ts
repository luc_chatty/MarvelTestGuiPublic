import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeroDisplay } from './component/heroDisplay/heroDisplay.component';
import { FirstPage } from './pages/first/first.component';
import { HeroModel } from './model/heroModel';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './service/api.service';


@NgModule({
  declarations: [
    AppComponent,
    HeroDisplay,
    FirstPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
