export class HeroModel {
    
id : String;    
name: String = '';
    
url: String = '';
description: String = '';

public constructor(id:String, name:String, url:String, description:String) {
      this.id = id;
      this.description = description;
      this.name = name;
      this.url = url;
}
      }